jQuery(document).ready(function($) {

	pm_event_check_duration();
	jQuery(document).on('change', '#_pm_event_meta_duration', function(event) {
		event.preventDefault();
		pm_event_check_duration();
	});

});


function pm_event_check_duration(){
	if( jQuery('#_pm_event_meta_duration').val() == 'all_day' ){
		jQuery('.cmb2-id--pm-event-meta-end-date').hide();
		jQuery('.cmb2-id--pm-event-meta-end-time').hide();
		jQuery('#_pm_event_meta_end_date').val('');
		jQuery('#_pm_event_meta_end_time').val('');
	}else{
		jQuery('.cmb2-id--pm-event-meta-end-date').show();
		jQuery('.cmb2-id--pm-event-meta-end-time').show();
		
	}
}