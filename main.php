<?php

/*

Plugin Name: PM Events Plugin
Description: Paradox Marketing's custom event plugin
Version: 1.0.0
Author: Melvin - Paradox Marketing
Text Domain: pm-events-plugin

*/

if ( ! defined( 'WPINC' ) ) {
    die;
}

require_once plugin_dir_path( __FILE__ ) . 'class/pm-events-plugin-class.php';



if( class_exists( 'PMEventsPlugin' ) ){
	$PMEventsPlugin = new PMEventsPlugin();
	$PMEventsPlugin->run();
}


// Activaton
register_activation_hook( __FILE__ , array( $PMEventsPlugin , 'activate' ) );

// Deactivate
register_deactivation_hook( __FILE__ , array( $PMEventsPlugin , 'deactivate' ) );

