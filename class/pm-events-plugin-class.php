<?php

if ( ! defined( 'ABSPATH' ) ) {
    die;
}

if( ! class_exists( 'PMEventsPlugin' ) ){
	
 	class PMEventsPlugin{

		protected $prefix;
		protected $version;

		function __construct(){
			require_once( plugin_dir_path( __FILE__ ).'../third-party/cmb2/init.php' );

			$this->prefix = '_pm_event_';
			$this->version = '1.0.0';
		}

		public function run(){
			add_action( 'init', array( $this, 'pm_event_custom_post' ), 10 , 0 );

			add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin' ) );
			add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_front' ) );

			add_action( 'cmb2_admin_init', array( $this, 'pm_event_post_meta' ) );

			add_shortcode( 'pm-event-shortcode', array( $this, 'pm_event_shortcode' ) );

			add_action('pm_event_layout_1', array( $this, 'pm_event_layout_1' ) );
			add_action('pm_event_layout_2', array( $this, 'pm_event_layout_2' ) );

		}

		public function register(){
			
		}

		
		public function activate(){
			// Flush rewrite rules
			flush_rewrite_rules();
		}

		public function deactivate(){
			// Flush rewrite rules
			flush_rewrite_rules();
		}

		public function uninstall(){

		}

		public function pm_event_shortcode( $atts ){
			$a = shortcode_atts( array(
				'layout' => 1,
			), $atts );

			ob_start();

			if( $a['layout'] == 1 ){
				do_action('pm_event_layout_1');
			}
			if( $a['layout'] == 2 ){

			}

			return ob_get_clean();
		}

		public function pm_event_layout_1(){
			$args = array(
				'post_type' => 'pm-event',
				'posts_per_page' => 4,
			);
			$qry = new WP_Query( $args ); ?>
			<style type="text/css">
				.pm-events{
					font-family: Arial, Helvetica, sans-serif;
					font-size: 18px;
					line-height: 28px;
				}
				.pm-events.standard:after{
					display: table;
					content: '';
					clear: both;
				}
				.pm-events.standard .pm-event-item{
					width: 50%;
					background-size: cover;
					float: left;
					padding: 15px;
				}
				.pm-events.standard .pm-event-item .pm-event-inner{
					background: rgba(255,255,255,.5);
					padding: 20px;
				}
				.pm-events.standard h2.pm-event-title,
				.pm-events.standard h2.pm-event-title a{
					font-size: 40px;
					margin: 0 0 10px 0;
					line-height: normal;
					text-align: center;
					color: #000;
					text-transform: uppercase;
					text-decoration: none;
					font-family: Georgia, serif;

				}
				.pm-events.standard h2.pm-event-title:before{
					content: '';
					display: none;
				}
				.pm-events.standard .pm-event-excerpt{
					text-align: center;
				}
			</style>
			<?php if ( $qry->have_posts() ) : ?>
				<div class="pm-events standard">
					<?php while ( $qry->have_posts() ) : $qry->the_post(); ?>
						<div class="pm-event-item" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);">
							<div class="pm-event-inner">
								<h2 class="pm-event-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
								<div class="pm-event-excerpt">
									<?php echo wp_trim_words( get_the_content(), 20, '... <a href="'.get_the_permalink().'">view event</a>' );?>
								</div>
								<div class="pm-event-date">
									
								</div>
							</div>
						</div>
					<?php endwhile; ?>
				</div>
				<?php wp_reset_postdata(); ?>
			<?php else : ?>
				<p><?php esc_html_e( 'Sorry, no event matched your criteria.' ); ?></p>
			<?php endif; ?>

			<?php
		}
		public function pm_event_layout_2(){
			echo "TEST";
		}

		public function pm_event_custom_post(){

			$labels = array(
				'name'                => _x( 'Events', 'Post Type General Name', 'pm-events-plugin' ),
				'singular_name'       => _x( 'Event', 'Post Type Singular Name', 'pm-events-plugin' ),
				'menu_name'           => __( 'Events', 'pm-events-plugin' ),
				'parent_item_colon'   => __( 'Parent Event', 'pm-events-plugin' ),
				'all_items'           => __( 'All Events', 'pm-events-plugin' ),
				'view_item'           => __( 'View Event', 'pm-events-plugin' ),
				'add_new_item'        => __( 'Add New Event', 'pm-events-plugin' ),
				'add_new'             => __( 'Add New Event', 'pm-events-plugin' ),
				'edit_item'           => __( 'Edit Event', 'pm-events-plugin' ),
				'update_item'         => __( 'Update Event', 'pm-events-plugin' ),
				'search_items'        => __( 'Search Event', 'pm-events-plugin' ),
				'not_found'           => __( 'Event Not Found', 'pm-events-plugin' ),
				'not_found_in_trash'  => __( 'Events Not found in Trash', 'pm-events-plugin' ),
			);
			$supports = array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields' );
			
			$args = array(
				'label'               => __( 'Events', 'pm-events-plugin' ),
				'description'         => __( 'Event news and reviews', 'pm-events-plugin' ),
				'labels'              => $labels,
				'supports'            => $supports,
				'hierarchical'        => false,
		        'public'              => true,
		        'show_ui'             => true,
		        'show_in_menu'        => true,
		        'show_in_nav_menus'   => true,
		        'show_in_admin_bar'   => true,
		        'menu_position'       => 5,
		        'can_export'          => true,
		        'has_archive'         => true,
		        'publicly_queryable'  => true,
		        'menu_icon'			  => 'dashicons-calendar',
		        'rewrite'			  => array( 'slug' => 'event' , 'with_front' => false ),
				'capability_type'     => 'page',
			);
			register_post_type( 'pm-event', $args );

			$labels = array(
				'name'              => _x( 'Event Categories', 'taxonomy general name', 'pm-events-plugin' ),
				'singular_name'     => _x( 'Event Category', 'taxonomy singular name', 'pm-events-plugin' ),
				'search_items'      => __( 'Search Event Categories', 'pm-events-plugin' ),
				'all_items'         => __( 'All Event Categories', 'pm-events-plugin' ),
				'parent_item'       => __( 'Parent Event Category', 'pm-events-plugin' ),
				'parent_item_colon' => __( 'Parent Event Category:', 'pm-events-plugin' ),
				'edit_item'         => __( 'Edit Event Category', 'pm-events-plugin' ),
				'update_item'       => __( 'Update Event Category', 'pm-events-plugin' ),
				'add_new_item'      => __( 'Add New Event Category', 'pm-events-plugin' ),
				'new_item_name'     => __( 'New Event Category Name', 'pm-events-plugin' ),
				'menu_name'         => __( 'Event Category', 'pm-events-plugin' ),
			);

			$args = array(
				'hierarchical'      => true,
				'labels'            => $labels,
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'event-category' ),
			);

			register_taxonomy( 'pm-event-category', array( 'pm-event' ), $args );

			$labels = array(
				'name'                       => _x( 'Event Tags', 'taxonomy general name', 'pm-events-plugin' ),
				'singular_name'              => _x( 'Event Tag', 'taxonomy singular name', 'pm-events-plugin' ),
				'search_items'               => __( 'Search Event Tags', 'pm-events-plugin' ),
				'popular_items'              => __( 'Popular Event Tags', 'pm-events-plugin' ),
				'all_items'                  => __( 'All Event Tags', 'pm-events-plugin' ),
				'parent_item'                => null,
				'parent_item_colon'          => null,
				'edit_item'                  => __( 'Edit Event Tag', 'pm-events-plugin' ),
				'update_item'                => __( 'Update Event Tag', 'pm-events-plugin' ),
				'add_new_item'               => __( 'Add New Event Tag', 'pm-events-plugin' ),
				'new_item_name'              => __( 'New Event Tag Name', 'pm-events-plugin' ),
				'separate_items_with_commas' => __( 'Separate Event Tags with commas', 'pm-events-plugin' ),
				'add_or_remove_items'        => __( 'Add or remove Event Tags', 'pm-events-plugin' ),
				'choose_from_most_used'      => __( 'Choose from the most used Event Tags', 'pm-events-plugin' ),
				'not_found'                  => __( 'No Event Tags found.', 'pm-events-plugin' ),
				'menu_name'                  => __( 'Event Tags', 'pm-events-plugin' ),
			);

			$args = array(
				'hierarchical'          => false,
				'labels'                => $labels,
				'show_ui'               => true,
				'show_admin_column'     => true,
				'update_count_callback' => '_update_post_term_count',
				'query_var'             => true,
				'rewrite'               => array( 'slug' => 'event-tag' ),
			);

			register_taxonomy( 'pm-event-tag', 'pm-event', $args );

			flush_rewrite_rules();
		}

		function pm_event_post_meta() {

			$cmb = new_cmb2_box( array(
				'id'            => $this->prefix.'_metabox_schedule',
				'title'         => __( 'Event Schedule', 'cmb2' ),
				'object_types'  => array( 'pm-event', ), // Post type
				'context'       => 'normal',
				'priority'      => 'high',
				'show_names'    => true, 
			) );
			$cmb->add_field( array(
				'name' => 'Time Zone:',
				'id'   => $this->prefix.'meta_timezone',
				'type' => 'select_timezone',
			) );
			$cmb->add_field( array(
				'name' => 'Date:',
				'id'   => $this->prefix.'meta_start_date',
				'type' => 'text_date',
				'date_format' => 'F j, Y',
				'timezone_meta_key' => $this->prefix.'meta_timezone',
				'attributes' => array(
					'data-datepicker' => json_encode( array(
						'yearRange' => '-0:+20',
					) ),
				),
			) );
			$cmb->add_field( array(
				'name' => 'Time:',
				'id'   => $this->prefix.'meta_start_time',
				'type' => 'text_time',
				'time_format' => 'g:i A',
				'timezone_meta_key' => $this->prefix.'meta_timezone',
			) );
			$cmb->add_field( array(
				'name'             => 'Event Duration',
				'id'               => $this->prefix.'meta_duration',
				'type'             => 'select',
				'show_option_none' => false,
				'options'          => array(
					'all_day'		=> __( 'All Day', 'cmb2' ),
					'date_range'   	=> __( 'Date Range', 'cmb2' ),
				),
			) );
			$cmb->add_field( array(
				'name' => 'End Date:',
				'id'   => $this->prefix.'meta_end_date',
				'type' => 'text_date',
				'date_format' => 'F j, Y',
				'timezone_meta_key' => $this->prefix.'meta_timezone',
				'attributes' => array(
					'data-datepicker' => json_encode( array(
						'yearRange' => '-0:+20',
					) ),
				),
			) );
			$cmb->add_field( array(
				'name' => 'End Time:',
				'id'   => $this->prefix.'meta_end_time',
				'type' => 'text_time',
				'time_format' => 'g:i A',
				'timezone_meta_key' => $this->prefix.'meta_timezone',
			) );
		}
		
		public function enqueue_admin(){
			wp_enqueue_style(
				$this->prefix.'admin_style',
				plugins_url( '../assets/css/admin-style.css', __FILE__ ),
				array(),
				$this->version,
				'all'
			);
			wp_enqueue_script(
				$this->prefix.'admin_js',
				plugins_url( '../assets/js/admin.js', __FILE__ ),
				array('jquery'),
				$this->version,
				false
			);
		}

		public function enqueue_front(){
			wp_enqueue_style(
				$this->prefix.'admin_style',
				plugins_url( '../assets/css/front-style.css', __FILE__ ),
				array(),
				$this->version,
				'all'
			);
			wp_enqueue_script(
				$this->prefix.'admin_js',
				plugins_url( '../assets/js/front.js', __FILE__ ),
				array('jquery'),
				$this->version,
				false
			);
		}

	}
}


